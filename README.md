Conicyt WSO2 Appgen
---
Script desarrollado para crear de forma automática las aplicaciones en WSO2 y suscribir las Apis asociadas.

Archivo de configuración *module.json*
---
```json

{
    "name": "TestApp",
    "version": "x.x.x",
    "description": "...",
    "dependencies": [
        {
            "apiName": "Elasticsearch",
            "apiVersion": "5.1.1",
            "tier": "Unlimited"
        },
        {
            "apiName": "RawBuscadorEquipos",
            "apiVersion": "1.0",
            "tier": "Unlimited"
        }
    ],
    "tokenTier": "50PerMin",
    "oauth2CallbackUrl": "https://..."
}
```

Ejemplo de uso
---

```js
mkdir tmp && cd tmp

cat << EOF > package.json
{
  "dependencies": {
    "conicyt-wso2-appgen": "git+https://bitbucket.org/conicytdev/conicyt-wso2-appgen.git"
  }
}
EOF

cat << EOF > index.js
const gen = require('conicyt-wso2-appgen') 
new gen.Client(
    {
      host: "DNS / IP:PORT",
      user: "xxx",
      password: process.argv[3]
    }
).createCredential(require('./' + process.argv[2]), function (res) {
    console.log(res.consumerKey, res.consumerSecret)
})
EOF

npm install
node index.js ../module.json xxx > keysecret

cat << EOF > config.yml
wso2:
  consumerkey:    "$(cat tmp/keysecret | awk '{print $1}')"
  consumersecret: "$(cat tmp/keysecret | awk '{print $2}')"  
EOF

rm -rf tmp

```