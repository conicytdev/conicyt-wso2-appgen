const q = require("q");
const _ = require("lodash");
const request = require("request");
const fs = require("fs");
const yaml = require('js-yaml');
var unzip = require('unzip');
var fstream = require('fstream');
var archiver = require('archiver');
var sleep = require('sleep');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

try {
    var wso2 = yaml.safeLoad(fs.readFileSync('config.yml', 'utf8'), {json: true});
} catch (e) {
    console.log("error:" + e.message);
    process.exit(1);
}

amLogin(wso2.am.url, wso2.am.username, wso2.am.password, 'publisher').then(
    function (cookie) {
        amGetAPIs(cookie).then(
            function (apis) {
                console.log('Listing APIs');

                var funcs = [];
                _.forEach(apis, function (api) {
                    funcs.push(function () {
                        return doit(api, wso2)
                    });
                });

                var result = q();
                funcs.forEach(function (f) {
                    result = result.then(f);
                });

                result.then(function () {
                    amLogout(wso2.am.url);

                }, function (error) {
                    amLogout(wso2.am.url);
                    console.log("error:", error)
                });

            },
            error);
    },
    error);


// funciones

function doit(api, wso2) {
    var defer = q.defer();

    amExportAPI(api, wso2.am.username, wso2.am.password).then(
        function () {
            var directory = api.name + '-' + api.version;
            sleep.sleep(2);

            var exportedApi = JSON.parse(fs.readFileSync(directory + '/Meta-information/api.json'));

            exportedApi['id'].providerName = 'arquitecto';
            if (!_.isUndefined(exportedApi.apiOwner)) {
                exportedApi['apiOwner'] = 'arquitecto';
            }

            fs.writeFileSync(directory + '/Meta-information/api.json', JSON.stringify(exportedApi));

            amLogin(wso2.amTarget.url, wso2.amTarget.username, wso2.amTarget.password, 'publisher').then(
                function (cookieTarget) {

                    amAPIExists(wso2.amTarget.url, exportedApi.id.apiName, exportedApi.id.version, exportedApi.id.providerName, cookieTarget).then(
                        function (exists) {
                            if (exists) {
                                console.log("updating " + api.name + "-" + api.version);

                                amUpdateAPI(wso2.amTarget.url, directory, cookieTarget).then(
                                    function () {
                                        amUpdateStatus(wso2.amTarget.url, exportedApi.id.apiName, exportedApi.id.version, exportedApi.id.providerName, cookieTarget).then(
                                            function () {
                                                console.log('Published: ', api.name, api.version);

                                                amLogout(wso2.amTarget.url).then(defer.resolve, defer.reject);
                                            },
                                            defer.reject);
                                    },
                                    defer.reject);
                            }
                            else {
                                console.log("creating " + api.name + "-" + api.version);

                                amAddAPI(wso2.amTarget.url, directory, cookieTarget).then(
                                    function (created) {
                                        if (created) {
                                            amUpdateStatus(wso2.amTarget.url, exportedApi.id.apiName, exportedApi.id.version, exportedApi.id.providerName, cookieTarget).then(
                                                function () {
                                                    console.log('Published: ', api.name + '-' + api.version);

                                                    amLogout(wso2.amTarget.url).then(defer.resolve, defer.reject)
                                                },
                                                defer.reject);
                                        }
                                        else {
                                            amLogout(wso2.amTarget.url).then(defer.resolve, defer.reject);
                                        }
                                    },
                                    defer.reject);
                            }

                            rmdirSync(__dirname + '/' + directory);
                        },
                        defer.reject);
                },
                defer.reject);
        },
        defer.reject);

    return defer.promise;
}

function error(error) {
    console.log('error: ', error);
}

function amLogin(wso2URL, username, password, site) {
    var defer = q.defer();
    request({
            agent: false,
            uri: wso2URL + "/" + site + "/site/blocks/user/login/ajax/login.jag",
            method: "POST",
            form: {
                action: "login",
                username: username,
                password: password
            }
        }, function (error, response, body) {
            var objBody = JSON.parse(body);

            if (objBody.error) {
                defer.resolve(objBody.message);
            }
            else {
                defer.resolve(response.headers['set-cookie']);
            }
        }
    );

    return defer.promise;
}

function amLogout(wso2URL) {
    var defer = q.defer();

    request({
            agent: false,
            uri: wso2URL + "/publisher/site/blocks/user/login/ajax/login.jag?action=logout",
            method: "GET",
            timeout: 10000,
            followRedirect: true,
            maxRedirects: 10
        }, function (error, response, body) {
            var objBody = JSON.parse(body);

            if (objBody.error) {
                defer.resolve(objBody.message);
            }
            else {
                defer.resolve();
            }
        }
    );

    return defer.promise;
}

function amGetApplications(cookie, callback) {
    request({
        uri: wso2.am.url + "/store/site/blocks/application/application-list/ajax/application-list.jag?action=getApplications",
        method: "GET",
        headers: {
            Cookie: cookie
        }
    }, callback);
}

function amGetAPIs(cookie) {
    var defer = q.defer();

    request({
            agent: false,
            uri: wso2.am.url + "/publisher/site/blocks/listing/ajax/item-list.jag?action=getAllAPIs",
            method: "GET",
            headers: {
                Cookie: cookie
            }
        }, function (error, response, body) {
            var objBody = JSON.parse(body);

            if (objBody.error) {
                defer.resolve(objBody.message);
            }
            else {
                defer.resolve(objBody.apis);
            }
        }
    );

    return defer.promise;
}

function amExportAPI(api, username, password) {
    var defer = q.defer();
    var b = new Buffer(username + ":" + password);
    var auth = 'Basic ' + b.toString('base64');

    var req = request({
        agent: false,
        uri: wso2.am.url + '/api-import-export-v0.9.1/export-api',
        method: 'GET',
        headers: {
            "Authorization": auth
        },
        qs: api
    })
        .pipe(unzip.Parse())
        .pipe(fstream.Writer(__dirname));

    req.on('close', function () {
        defer.resolve();
    });

    req.on('error', function (error) {
        defer.reject('error amExportAPI: ' + error);
    });

    return defer.promise;
}

function amAddAPI(wso2URL, apiDir, cookie) {
    var defer = q.defer();

    var swagger = JSON.parse(fs.readFileSync(apiDir + "/Meta-information/swagger.json", 'utf8'));
    var api = JSON.parse(fs.readFileSync(apiDir + "/Meta-information/api.json", 'utf8'));

    swagger = JSON.stringify(swagger);

    if (!api.endpointConfig) {
        defer.resolve(false);
    }
    else {

        var form = {
            action: 'addAPI',
            name: api.id.apiName,
            context: api.context,
            version: api.id.version,
            visibility: 'public',
            description: api.description,
            tags: api.tags,
            endpoint_config: api.endpointConfig,
            provider: api.id.providerName,
            thumbUrl: api.thumbUrl,
            http_checked: api.http_checked,
            https_checked: api.https_checked,
            tiersCollection: "Unlimited",
            swagger: swagger
        };

        request({
            agent: false,
            uri: wso2URL + "/publisher/site/blocks/item-add/ajax/add.jag",
            method: "POST",
            headers: {
                Cookie: cookie
            },
            timeout: 5000,
            form: form
        }, function (error, response, body) {
            try {
                var objBody = JSON.parse(body);

                if (objBody.error) {
                    defer.reject('error amAddAPI:' + objBody.message)
                }
                else {
                    defer.resolve(true);
                }
            } catch (e) {
                console.log(error, response, body);
                defer.reject('error amAddAPI:' + error);

            }

        });
    }

    return defer.promise;

}

function amAPIExists(wso2URL, apiName, apiVersion, apiProvider, cookie) {
    var defer = q.defer();

    request({
        agent: false,
        uri: wso2URL + "/publisher/site/blocks/listing/ajax/item-list.jag",
        method: "POST",
        headers: {
            Cookie: cookie
        },
        form: {
            action: 'getAPI',
            provider: apiProvider,
            name: apiName,
            version: apiVersion
        }
    }, function (error, response, body) {
        var objBody = JSON.parse(body);

        if (objBody.error) {
            defer.resolve(false);
        }
        else {
            defer.resolve(!_.isUndefined(objBody.api));
        }
    });

    return defer.promise;
}

function amUpdateAPI(wso2URL, apiDir, cookie) {
    var defer = q.defer();

    var swagger = JSON.parse(fs.readFileSync(apiDir + "/Meta-information/swagger.json", 'utf8'));
    var api = JSON.parse(fs.readFileSync(apiDir + "/Meta-information/api.json", 'utf8'));

    swagger = JSON.stringify(swagger);

    var form = {
        action: 'updateAPI',
        name: api.id.apiName,
        version: api.id.version,
        provider: api.id.providerName,
        context: _.replace(api.context, '/' + api.id.version, ''),
        visibility: 'public',
        thumbUrl: api.thumbUrl,
        description: api.description,
        tags: api.tags,
        endpoint_config: api.endpointConfig,
        http_checked: api.http_checked,
        https_checked: api.https_checked,
        tiersCollection: api.tiersCollection,
        swagger: swagger
    };

    request({
        agent: false,
        uri: wso2URL + "/publisher/site/blocks/item-add/ajax/add.jag",
        method: "POST",
        headers: {
            Cookie: cookie
        },
        form: form
    }, function (error, response, body) {
        var objBody = JSON.parse(body);

        if (objBody.error) {
            defer.reject(objBody.message)
        }
        else {
            defer.resolve();
        }
    });

    return defer.promise;
}

function amUpdateStatus(wso2URL, apiName, apiVersion, apiProvider, cookie) {
    var defer = q.defer();

    request({
        agent: false,
        uri: wso2URL + "/publisher/site/blocks/life-cycles/ajax/life-cycles.jag",
        method: "POST",
        headers: {
            Cookie: cookie
        },
        form: {
            action: 'updateStatus',
            name: apiName,
            version: apiVersion,
            provider: apiProvider,
            status: 'PUBLISHED',
            publishToGateway: 'true',
            requireResubscription: 'true'
        }
    }, function (error, response, body) {
        var objBody = JSON.parse(body);

        if (objBody.error) {
            defer.reject('Error amUpdateStatus:' + objBody.message);
        }
        else {
            defer.resolve();
        }
    });

    return defer.promise;
}

function rmdirSync(dir) {
    var s = fs.lstatSync(dir);
    if (s.isFile())
        fs.unlinkSync(dir);
    if (!s.isDirectory())
        return;

    var fileArr = fs.readdirSync(dir);
    for (f in fileArr)
        rmdirSync(dir + '/' + fileArr[f]);

    fs.rmdirSync(dir);
}